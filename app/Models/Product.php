<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function format()
    {
        return [
            'product_id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'last_updated' => $this->updated_at->diffForHumans(),
        ];
    }
}
