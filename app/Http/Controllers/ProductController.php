<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /*
     * Inject repository
     */

    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        return $this->productRepository->all();
    }

    public function store()
    {
        return $this->productRepository->store();
    }

    public function show($productId)
    {
        return $this->productRepository->findById($productId);
    }

    public function update($productId)
    {
        return $this->productRepository->update($productId);
    }

    public function destroy($productId)
    {
        $this->productRepository->delete($productId);
        return redirect('/products');
    }
}
