<?php

namespace App\Repositories;

interface ProductRepositoryInterface
{
    public function all();

    public function store();

    public function findById($productId);

    public function update($productId);

    public function delete($productId);
}
