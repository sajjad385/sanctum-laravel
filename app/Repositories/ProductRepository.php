<?php


namespace App\Repositories;


use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductRepository implements ProductRepositoryInterface
{
    public function all()
    {
//        Call repository format
//        return Product::orderBy('title')
//            ->get()
//            ->map(function ($product) {
//                return $this->format($product);
//            });
//        call model format
//        return Product::orderBy('title')
//            ->get()
//            ->map(function ($product) {
//                return $product->format();
//            });
        /**
         *
         */
        return Product::orderBy('title')
            ->get()
            ->map->format();

    }

    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'title' => 'required',
            'price' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        return Product::create(request()->toArray());
    }

    public function findById($productId)
    {
        $product = Product::where('id', $productId)->firstOrFail();
        return $product->format();
//        return $this->format($product);
    }


    public function update($productId)
    {
        $product = Product::where('id', $productId)->firstOrFail();
        $product->update(request()->all());
        return $product;

    }

    public function delete($productId)
    {
        $product = Product::where('id', $productId)->delete();

    }

    protected function format($product): array
    {
        return [
            'product_id' => $product->id,
            'title' => $product->title,
            'description' => $product->description,
            'last_updated' => $product->updated_at->diffForHumans(),
        ];
    }
}
