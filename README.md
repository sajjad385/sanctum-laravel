#Sanctum Laravel - Api Authentication

This is an Laravel sanctum authentication Build by <a href="">Sajjad</a>

## Installation Process

```
- git clone https://gitlab.com/sajjad385/sanctum-laravel.git
- cp .env.example .env
- Set Database Connection
- composer install
- php artisan key:generate
- php artisan migrate

```

